import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { UserService } from '../../../src/app/services/user.service';
import { CommonService } from '../../../src/app/services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public step1Show = true;
  public step2Show = false;
  public userName: any;
  public password: any;
  public submittedS1 = false;
  public submittedS2  = false;
  public loginFormS1: FormGroup;
  public loginFormS2: FormGroup;
  public responseError: string;
  public name: string;
  public userDetails: any;

  private submissionArray: any;

  constructor( private router: Router,
               private fb: FormBuilder,
               private userService: UserService,
               private commService: CommonService,
               private toastr: ToastrService ) { }

  ngOnInit() {
    this.loginFormS1 = this.fb.group({
      userName: ['', Validators.required]
    });

    this.loginFormS2 = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get step1() { return this.loginFormS1.controls; }
  get step2() { return this.loginFormS2.controls; }

  createAccount() {
    this.router.navigate(['signup']);
  }

  forgetName() {
    this.router.navigate(['forget']);
  }

  // Check step1
  step1Check() {
    this.submittedS1 = true;

    if (this.loginFormS1.valid) {
      this.submissionArray = {};
      this.submissionArray.emailorphone = this.loginFormS1.value.userName;
     // console.log(this.submissionArray);

      this.userService.checkUser(this.submissionArray).subscribe((data: any) => {
        if (data.responseCode === 201) {
          this.responseError = data.responseDetails;
        } else if (data.responseCode === 200) {
          this.step1Show =  false;
          this.step2Show =  true;
          this.userName  =  this.loginFormS1.value.userName;
          this.name      =  data.userDetails.first_name;
        } else {
          this.responseError = data.responseDetails;
        }
      }, error => {
         this.responseError = 'Something went wrong!';
      });
    }
  }

  onSubmit() {
      this.submittedS2 = true;

      if (this.loginFormS1.valid) {
        this.submissionArray = {};
        this.submissionArray.emailorphone = this.userName;
        this.submissionArray.password     = this.loginFormS2.value.password;

        this.userService.login(this.submissionArray).subscribe((data: any) => {
            if (data.responseCode === 201) {
              this.responseError = data.responseDetails;
            } else if (data.responseCode === 200) {
              this.toastr.success('Login Successfull!', '');
              this.commService.setSession('user', data.userDetails);
              this.router.navigate(['/home']);
            } else {
              this.responseError = data.responseDetails;
            }
          }, error => {
            this.responseError = 'Something went wrong!';
          });
      }
  }
}
