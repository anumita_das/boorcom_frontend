import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.scss']
})
export class ForgetComponent implements OnInit {

  public stepOne = true;
  public stepTwo = false;
  public stepThree = false;

  // form validation
    forgetOneForm: FormGroup;
    forgetTwoForm: FormGroup;

    submitted = false;
    constructor(private formBuilder: FormBuilder) { }


  ngOnInit() {
    this.forgetOneForm = this.formBuilder.group({
        firstName: ['', Validators.required]
        // lastName: ['', Validators.required],
        // email: ['', [Validators.required, Validators.email]],
        // password: ['', [Validators.required, Validators.minLength(6)]]
    });

    this.forgetTwoForm = this.formBuilder.group({
      code: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.forgetOneForm.controls; }
   get f2() { return this.forgetTwoForm.controls; }

   forgetOne() {
    this.submitted = true;
    if (this.forgetOneForm.valid) {
      this.stepOne = false;
      this.stepTwo = true;
      this.stepThree = false;
     }

    console.log(this.forgetOneForm.value);
}


forgetTwo() {
  this.submitted = true;
  if (this.forgetTwoForm.valid) {
   this.stepOne = false;
   this.stepTwo = false;
   this.stepThree = true;
  }
  console.log(this.forgetTwoForm.value);
}



  // step1(){
  //   this.stepOne = true;
  //   this.stepTwo = false;
  //   this.stepThree = false;
  // }

  // step2(){
  //   this.stepOne = false;
  //   this.stepTwo = true;
  //   this.stepThree = false;
  // }

  // step3(){
  //   this.stepOne = false;
  //   this.stepTwo = false;
  //   this.stepThree = true;
  // }

}
