import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../../../src/app/services/common.service';
import {Router} from '@angular/router';
import {UserService} from '../../../src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ValidatePassword } from '../../../src/app/validators/password.validator';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

    constructor(private formBuilder: FormBuilder,
                private commonService: CommonService,
                private router: Router,
                private userService: UserService,
                private toastr: ToastrService
              ) {

    }

     // convenience getter for easy access to form fields
     get s1() { return this.signupOneForm.controls; }
     get s2() { return this.signupTwoForm.controls; }
     get s3() { return this.signupThreeForm.controls; }
     get s4() { return this.signupFourForm.controls; }
     get s5() { return this.signupFiveForm.controls; }
  show = false;
  private otp: number;
  private callPrefix: string;
  private phoneNumber: any;

  public stepOne = true;
  public stepTwo = false;
  public stepThree = false;
  public stepFour = false;
  public stepFive = false;
  public stepSix = false;
  public stepSeven = false;
  public result: any;
  public countryList: any;
  public submissionArray: any;
  public responseError: string;

  // public FormControl: any;

  // Image upload start
  public imagePath;
  imgURL: any;
  public message: string;
    // image upload end

    // form validation

    signupOneForm: FormGroup;
    signupTwoForm: FormGroup;
    signupThreeForm: FormGroup;
    signupFourForm: FormGroup;
    signupFiveForm: FormGroup;
    submitted   = false;

    preview(files) {
      if (files.length === 0) {
        return;
      }

      const mimeType = files[0].type;
      if (mimeType.match(/image\/*/) == null) {
        this.message = 'Only images are supported.';
        return;
      }

      const reader = new FileReader();
      this.imagePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      };
    }

    ngOnInit() {

      this.commonService.getAllCountries().subscribe((result: any) => {
        if (result.responseCode === 200) {
          this.countryList  = result.data;
        }
      });

      this.signupOneForm = this.formBuilder.group({
        call_prefix: ['91', [Validators.required]],
        username: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(11)]],
      });

      this.signupTwoForm = this.formBuilder.group({
        password: ['', [Validators.required, ValidatePassword]]
      });

      this.signupThreeForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]]
      });

      this.signupFourForm = this.formBuilder.group({
        otpCode: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]]
      });

      this.signupFiveForm = this.formBuilder.group({
        // otpCode: ['', Validators.required]
      });
    }

    signupOne() {
      this.submitted = true;
      if (this.signupOneForm.valid) {
        this.submissionArray = {};
        this.submissionArray.emailorphone = this.signupOneForm.value.username;
        this.callPrefix  = this.signupOneForm.value.call_prefix;
        this.phoneNumber = this.signupOneForm.value.username;

        this.userService.checkUser(this.submissionArray).subscribe((data: any) => {
        if (data.responseCode !== 200) {
            this.responseError = '';
            this.submitted     = false;
            this.stepOne       = false;
            this.stepTwo       = true;
            this.stepThree     = false;
            this.stepFour      = false;
            this.stepFive      = false;
            this.stepSix       = false;
            this.stepSeven     = false;
          } else {
            this.responseError = 'This phone number already exits!';
          }
        }, error => {
          this.responseError = 'Something went wrong!';
        });
      }
    }

    signupTwo() {
      this.submitted = true;
      if (this.signupTwoForm.valid) {
        this.submitted = false;
        this.stepOne = false;
        this.stepTwo = false;
        this.stepThree = true;
        this.stepFour = false;
        this.stepFive = false;
        this.stepSix = false;
        this.stepSeven = false;
      }
    }

    signupThree() {
      this.submitted = true;
      if (this.signupThreeForm.valid) {

        this.submissionArray = {};
        this.submissionArray.emailorphone = this.signupThreeForm.value.email;

        this.userService.checkUser(this.submissionArray).subscribe((data: any) => {
          if (data.responseCode !== 200) {
            this.responseError                  = '';
            this.submissionArray                = {};
            this.submissionArray.call_prefix = this.callPrefix;
            this.submissionArray.phone       = this.phoneNumber;

            this.commonService.getOtp(this.submissionArray).subscribe((result: any) => {
                  this.otp  =   result.otp;
            });

            this.submitted  = false;
            this.stepOne    = false;
            this.stepTwo    = false;
            this.stepThree  = false;
            this.stepFour   = true;
            this.stepFive   = false;
            this.stepSix    = false;
            this.stepSeven  = false;
          } else {
            this.responseError = 'This email already exits!';
          }
        }, error => {
            this.responseError = 'Something went wrong!';
        });
      }
    }

    signupFour() {
      this.submitted = true;
      const otpValue = this.signupFourForm.value.otpCode;

      if (otpValue !== this.otp) {
        this.responseError = 'Invalid OTP!';
      } else {
        this.responseError = '';
      }

      if (this.signupFourForm.valid && !this.responseError) {
        // this.submitted = false;
        this.stepOne = false;
        this.stepTwo = false;
        this.stepThree = false;
        this.stepFour = false;
        this.stepFive = true;
        this.stepSix = false;
        this.stepSeven = false;
      }
    }

    signupFive() {
      this.submitted = true;
      if (this.signupFiveForm.valid) {
        this.submitted = false;
        this.stepOne = false;
        this.stepTwo = false;
        this.stepThree = false;
        this.stepFour = false;
        this.stepFive = false;
        this.stepSix = true;
        this.stepSeven = false;
      }
    }

    finish() {
        this.submissionArray                = {};
        this.submissionArray.country_code = this.callPrefix;
        this.submissionArray.phone       = this.phoneNumber;
        this.submissionArray.first_name  = this.signupThreeForm.value.firstName;
        this.submissionArray.last_name   = this.signupThreeForm.value.lastName;
        this.submissionArray.email       = this.signupThreeForm.value.email;
        this.submissionArray.password    = this.signupTwoForm.value.password;
        this.submissionArray.device_type = 'web';
        this.submissionArray.device_token = 'abcd';

        this.userService.signup(this.submissionArray).subscribe((result: any) => {
          if (result.responseCode === 200) {
              this.toastr.success('Registration Successfull!', '');
              this.commonService.setSession('user', result.userDetails);
              this.router.navigate(['/home']);
          } else {
            this.toastr.error('Error in operation!', '');
            this.responseError = 'Error in operation!';
          }
        });
    }

    cancelRegistration() {
      this.router.navigate(['/login']);
    }

    goStep1() {
      this.stepOne = true;
      this.stepTwo = false;
      this.stepThree = false;
      this.stepFour = false;
      this.stepFive = false;
      this.stepSix = false;
      this.stepSeven = false;
    }

   goStep2() {
     this.stepOne = false;
     this.stepTwo = true;
     this.stepThree = false;
     this.stepFour = false;
     this.stepFive = false;
     this.stepSix = false;
     this.stepSeven = false;
   }

   goStep3() {
     this.stepOne   = false;
     this.stepTwo   = false;
     this.stepThree = true;
     this.stepFour  = false;
     this.stepFive  = false;
     this.stepSix   = false;
     this.stepSeven = false;
   }

  // step4(){
  //   this.stepOne = false;
  //   this.stepTwo = false;
  //   this.stepThree = false;
  //   this.stepFour = true;
  //   this.stepFive = false;
  //   this.stepSix = false;
  //   this.stepSeven = false;
  // }

  gotoStep5() {
     this.stepOne = false;
     this.stepTwo = false;
     this.stepThree = false;
     this.stepFour = false;
     this.stepFive = true;
     this.stepSix = false;
     this.stepSeven = false;
   }

   step6() {
    this.stepOne = false;
    this.stepTwo = false;
    this.stepThree = false;
    this.stepFour = false;
    this.stepFive = false;
    this.stepSix = true;
    this.stepSeven = false;
  }

   gotoStep6() {
    this.stepOne = false;
    this.stepTwo = false;
    this.stepThree = false;
    this.stepFour = false;
    this.stepFive = false;
    this.stepSix = true;
    this.stepSeven = false;
  }

  step7() {
    this.stepOne = false;
    this.stepTwo = false;
    this.stepThree = false;
    this.stepFour = false;
    this.stepFive = false;
    this.stepSix = false;
    this.stepSeven = true;
  }
}


