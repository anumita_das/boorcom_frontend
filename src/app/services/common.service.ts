import { Injectable } from '@angular/core';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private baseUrl = environment.api;
  private httpOptions: any;

  public submissionArray: any;
  public userDetails: any;

  constructor(private http: HttpClient, public session: SessionStorageService) {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  }

  setSession(key, value, expired: number = 86400) {
    this.session.set(key, value, expired, 's');
  }

  removeSession(key) {
    this.session.remove(key);
  }

  getSession(key) {
    return this.session.get(key);
  }

  clearSession() {
    this.session.clear();
  }

  logout() {
    /*** Session Storage ***/
    this.removeSession('user');
  }

  getAllCountries() {
		return this.http.get(this.baseUrl + 'country/getAllCountries', this.httpOptions);
  }

  getOtp(form) {
		return this.http.post(this.baseUrl + 'user/resendOtp', form, this.httpOptions);
  }
}
