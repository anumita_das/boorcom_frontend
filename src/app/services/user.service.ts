import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = environment.api;
  private httpOptions: any;

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  }

  // Check whether user exits or not in database
  checkUser(form) {
    return this.http.post(this.baseUrl + 'user/checkUser', form, this.httpOptions);
  }

  login(form) {
    return this.http.post(this.baseUrl + 'user/userLogin', form, this.httpOptions);
  }

  signup(form) {
    return this.http.post(this.baseUrl + 'user/addUser', form, this.httpOptions);
  }
}
