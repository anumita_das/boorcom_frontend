import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from '../app/app.shared.module';
import { HomeComponent } from '../layout/home/home.component';
import { LoginComponent } from '../auth/login/login.component';
import { SignupComponent } from '../auth/signup/signup.component';
import { ForgetComponent } from '../auth/forget/forget.component';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard, DisableAuthGuard } from '../app/guards/auth.guard';

@NgModule({
  declarations: [
    // BrowserAnimationsModule,
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    ForgetComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgScrollbarModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 20000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      messageClass: 'toast-message',
    }),
  ],
  providers: [AuthGuard, DisableAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
