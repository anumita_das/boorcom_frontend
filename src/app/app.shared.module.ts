// common angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';


// custom module
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';

// custom component
import { HeaderComponent } from '../component/header/header.component';
import { FooterComponent } from '../component/footer/footer.component';
import { ChatSidebarComponent } from '../component/chat-sidebar/chat-sidebar.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        AngularFontAwesomeModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot()
    ],
    exports: [
        CommonModule,
        FormsModule,
        RouterModule,
        ReactiveFormsModule,
        HeaderComponent,
        FooterComponent,
        ChatSidebarComponent,
        AngularFontAwesomeModule,
        FormsModule,
        BsDropdownModule,
        TabsModule
    ],
    declarations: [
        HeaderComponent,
        FooterComponent,
        ChatSidebarComponent
    ],
    providers: [
        // CustomService
    ],
    entryComponents: [
    ]
})

export class SharedModule { }
