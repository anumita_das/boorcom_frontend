import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../layout/home/home.component';
import { LoginComponent } from '../auth/login/login.component';
import { SignupComponent } from '../auth/signup/signup.component';
import { ForgetComponent } from '../auth/forget/forget.component';
import { AuthGuard, DisableAuthGuard } from '../app/guards/auth.guard';
import { Router } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full', canActivate: [DisableAuthGuard] },
  { path: 'login', component: LoginComponent, pathMatch: 'full', canActivate: [DisableAuthGuard]},
  { path: 'signup', component: SignupComponent, pathMatch: 'full', canActivate: [DisableAuthGuard]},
  { path: 'forget', component: ForgetComponent, pathMatch: 'full', canActivate: [DisableAuthGuard]},
  { path: 'home', component: HomeComponent, pathMatch: 'full', canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
