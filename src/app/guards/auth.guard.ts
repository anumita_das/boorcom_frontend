import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CommonService } from '../../../src/app/services/common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private commService: CommonService ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    /*** Session Storage ***/
    if (this.commService.getSession('user')) {
      const user = this.commService.getSession('user');

      if (user.token) {
        // logged in so return true
        return true;
      } else {
        this.router.navigate(['/login']);
      }
    } else {
      this.router.navigate(['/login']);
    }
    /*** Session Storage ***/
    this.router.navigate(['/login']);
    return false;
  }
}

@Injectable()
export class DisableAuthGuard implements CanActivate {

  private userData: any = {};

    constructor(
        private router: Router,
        private commService: CommonService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        /*** Session Storage ***/
        if (this.commService.getSession('user')) {
            const userData =  this.commService.getSession('user');

            if (userData.token) {
                this.router.navigate(['home']);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }

        // Not logged in so redirect to login page with the return url
        this.router.navigate(['home']);
        return false;
    }
}
