import { AbstractControl, Validator } from '@angular/forms';
export function ValidatePassword(control: AbstractControl) {
    if (!/^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,20}$/.test(control.value)) {
      return { validPassword: true };
    }
    return null;
}
