import { Component, OnInit, NgZone, AfterViewInit, ViewChild, TemplateRef  } from '@angular/core';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

    show = false;
    disabled = false;
    compact = true;
    invertX = false;
    invertY = false;
    login = true;
    // shown : native : any;

    constructor() {

    }

    ngOnInit() {
        this.login = true;
    }

    toggle() {
        this.show = !this.show;
    }

}
