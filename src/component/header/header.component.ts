import {Component, OnInit, NgZone, AfterViewInit, ViewChild, TemplateRef, Input} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, FormGroupDirective, NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../../src/app/services/common.service';
import { ToastrService } from 'ngx-toastr';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent implements OnInit {

  logo: any = 'assets/images/logo.png';
  name = '';

  @Input() isLogin: boolean;

  constructor(
    private commonService: CommonService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    const user = this.commonService.getSession('user');
    if (user) {
    this.name = user.name;
    }
  }

  logout() {
    this.toastr.success('Logout Successfull!', '');
    this.commonService.logout();
    this.router.navigate(['/login']);
  }
}
