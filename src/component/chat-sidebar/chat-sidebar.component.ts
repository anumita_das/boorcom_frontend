import {Component, OnInit, NgZone, AfterViewInit, ViewChild, TemplateRef} from '@angular/core';
import { Router } from '@angular/router';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-chatsidebar',
  templateUrl: './chat-sidebar.component.html',
  styleUrls: ['./chat-sidebar.component.scss'],
})

export class ChatSidebarComponent implements OnInit {

  chatList: any;
  callList: any;
  contactList: any;

  constructor(
    //
  ) {}

  ngOnInit() {
    this.chatList = [
      {
        name: 'Saymond Stafell',
        msg: 'Much Needed',
        pic: 'assets/images/user-1.png',
        time: '10:29AM'
      },
      {
        name: 'Linda Jacobin',
        msg: 'Thanks!',
        pic: 'assets/images/user-2.png',
        time: '10:29AM'
      },
      {
        name: 'Jany Thomson',
        msg: 'Thanks!',
        pic: 'assets/images/user-3.png',
        time: 'Monday'
      },
      {
        name: 'Oliva Mariona',
        msg: 'Okay, Got it!',
        pic: 'assets/images/user-4.png',
        time: 'Friday'
      },
      {
        name: 'Saymond Stafell',
        msg: 'Much Needed',
        pic: 'assets/images/user-1.png',
        time: '16 Feb, 19'
      },
      {
        name: 'Saymond Stafell',
        msg: 'Much Needed',
        pic: 'assets/images/user-2.png',
        time: '10:29AM'
      }
    ],

    this.callList = [
      {
        name: 'Saymond Stafell',
        lastcall: 'Thusrday at 11:49',
        pic: 'assets/images/user-1.png'
      },
      {
        name: 'Saymond Stafell',
        lastcall: '21-01-2019',
        pic: 'assets/images/user-2.png'
      },
      {
        name: 'Saymond Stafell',
        lastcall: '21-01-2019',
        pic: 'assets/images/user-3.png'
      },
      {
        name: 'Saymond Stafell',
        lastcall: '21-01-2019',
        pic: 'assets/images/user-4.png'
      },
      {
        name: 'Saymond Stafell',
        lastcall: '21-01-2019',
        pic: 'assets/images/user-1.png'
      },
      {
        name: 'Saymond Stafell',
        lastcall: '21-01-2019',
        pic: 'assets/images/user-2.png'
      }
    ],
    this.contactList = [
      {
        name: 'Saymond Stafell',
        pic: 'assets/images/user-4.png'
      },
      {
        name: 'Saymond Stafell',
        pic: 'assets/images/user-4.png'
      },
      {
        name: 'Saymond Stafell',
        pic: 'assets/images/user-4.png'
      },
      {
        name: 'Saymond Stafell',
        pic: 'assets/images/user-4.png'
      },
      {
        name: 'Saymond Stafell',
        pic: 'assets/images/user-4.png'
      },
      {
        name: 'Saymond Stafell',
        pic: 'assets/images/user-4.png'
      }
    ];
  }

}
