export const environment = {
  production      : true,
  api             : 'http://111.93.177.61:5000/',
  upload_url      : 'http://111.93.177.61:5000/storage/',
  frontend_url    : 'http://111.93.177.61/boorcom_frontend',
  siteTitle		    : 'Boorcom - '
};
